mod lib;
mod util;

use std::env;

const VERSION_MSG: &'static str = "version 0.0.1; a.k.a. 'not even alpha'";
const HELP_MSG: &'static str = r#"
commands:
    help        prints this message and exits
    version     gives the version of this programm and exits

Every error is printed with the suffix '::'. Debug information is 
preceded with an '?:' (only printed when compiled with the feature 
'debug' being enabled).
"#;


fn main() {
    let args: Vec<String> = env::args().collect();
    
    #[cfg(feature = "debug")]
    eprintln!("?: list of arguments: {:?}", args);

    if args.len() < 2 {
        eprintln!(":: Missing parameters.\n:: => try `{} help` to get a list of commands", args[0]);
    } else if args[1] == String::from("version") {
        println!("{} {}", args[0], VERSION_MSG);
        ::std::process::exit(0);
    } else if args[1] == String::from("help") {
        println!("usage:\n    {} [command]\n\n{}", args[0], HELP_MSG);
    } else {
        eprintln!(":: Unknown command/parameter: '{}'\n:: => try `{} help` to get a list of commands", args[1], args[0]);
        ::std::process::exit(1);
    }

}

